var gulp = require('gulp');
var compass = require('gulp-compass');
var uglify = require('gulp-uglify');
var pump = require('pump');

gulp.task('styles', function() {
   gulp.src('assets/scss/**/*.scss')
       .pipe(compass({
           config_file: 'assets/config.rb',
           css: 'assets/css',
           sass: 'assets/scss',
           sourcemap: true
       }))
       // .on('error', function() {
       //     this.emit('end');
       // })
       .pipe(gulp.dest('assets/css'));
});

gulp.task('compress', function (cb) {
    pump([
        gulp.src('assets/js/**/*.js'),
        uglify({
            mangle: false
        }),
        gulp.dest('assets/js/min/')
    ])
});

gulp.task('default', ['styles'], function() {
    gulp.watch('assets/scss/**/*.scss', ['styles']);
});